package com.gsuk.zadaca;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class HallofFame extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hallof_fame);
    }

    public void imageClick(View view) {
        String klub = "";
        switch(view.getId()) {
            case R.id.ronaldinho:
                klub = "Barcelona";
                break;
            case R.id.zidan:
                klub = "REAL MADRID! <3";
                break;
            case R.id.imageView2:
                klub = "Milan";
                break;
        }
        Toast.makeText(getApplicationContext(),"Najpoznatiji klub za koji je igrao: " + klub,Toast.LENGTH_LONG).show();


    }
}
